import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {SudokuTableComponent} from "./components/sudoku-table/sudoku-table.component";
import {FormsModule} from "@angular/forms";
import {SudokuSolverService} from "./services/sudoku-solver.service";
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        SudokuTableComponent
      ],
      imports: [
        FormsModule
      ],
      providers: [
        SudokuSolverService
      ]

    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Sudoku solver'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Sudoku solver');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Sudoku solver');
  }));
});
