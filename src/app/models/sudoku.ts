export class Sudoku {

  constructor(
    public sudokuMassive: number[][],
    public sudokuBlocksMassive: number[][] = [],
    public sudokuValidOptions: number[][][] = []
  ) {
    if (sudokuMassive.length === 0) {
      for (let i = 0; i < 9; i++) {
        this.sudokuMassive[i] = [];

        for (let j = 0; j < 9; j++) {
          this.sudokuMassive[i][j] = null;
        }
      }
    } else {
      this.createSudokuBlocksMassive();
    }
  }

  public updateSudoku(x: number, y: number, value: number) {
    this.sudokuMassive[x][y] = value;
    this.createSudokuBlocksMassive();
  }

  public createSudokuBlocksMassive() {
    let sudokuBlocks = [];

    let startingRow = 0;
    let startingColumn = 0;

    for (let blockNumber = 0; blockNumber < 9; blockNumber++) {
      sudokuBlocks[blockNumber] = [];

      let row = startingRow;

      for (let rowInBlock = 0; rowInBlock < 3; rowInBlock++) {
        let column = startingColumn;

        for (let columnInBlock = 0; columnInBlock < 3; columnInBlock++) {
          sudokuBlocks[blockNumber][rowInBlock * 3 + columnInBlock] = this.sudokuMassive[row][column];
          column++;
        }
        row++;
      }

      if (startingColumn != 6) {
        startingColumn = startingColumn + 3;
      } else {
        startingColumn = 0;
        startingRow = startingRow + 3;
      }
    }

    this.sudokuBlocksMassive = sudokuBlocks;
  }
}
