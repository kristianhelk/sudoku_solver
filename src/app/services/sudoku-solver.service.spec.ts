import {TestBed} from '@angular/core/testing';

import {SudokuSolverService} from './sudoku-solver.service';
import {Sudoku} from '../models/sudoku'

describe('SudokuSolverService', () => {
  let service: SudokuSolverService;
  const CORRECT_SUDOKU = [
    [0, 0, 0, 4, 0, 6, 0, 0, 0],
    [0, 0, 7, 1, 0, 5, 4, 0, 0],
    [0, 6, 0, 0, 0, 0, 0, 9, 0],
    [8, 3, 0, 0, 0, 0, 0, 4, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [2, 5, 0, 0, 0, 0, 0, 3, 9],
    [0, 8, 0, 0, 0, 0, 0, 2, 0],
    [0, 0, 4, 3, 0, 2, 5, 0, 0],
    [0, 0, 0, 7, 0, 1, 0, 0, 0]
  ];
  const BAD_SUDOKU = [
    [0, 0, 0, 4, 0, 6, 0, 0, 0],
    [0, 0, 7, 1, 0, 5, 4, 0, 0],
    [0, 6, 0, 0, 0, 0, 0, 9, 0],
    [8, 3, 0, 0, 3, 0, 0, 4, 7],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [2, 5, 0, 0, 0, 0, 0, 3, 9],
    [0, 8, 0, 0, 0, 0, 0, 2, 0],
    [0, 0, 4, 3, 0, 2, 5, 0, 0],
    [0, 0, 0, 7, 0, 1, 0, 0, 0]
  ];
  const UNSOLVABLE_SUDOKU = [
    [0, 0, 9, 0, 2, 8, 7, 0, 0],
    [8, 0, 6, 0, 0, 4, 0, 0, 5],
    [0, 0, 3, 0, 0, 0, 0, 0, 4],
    [6, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 2, 0, 7, 1, 3, 4, 5, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 2],
    [3, 0, 0, 0, 0, 0, 5, 0, 0],
    [9, 0, 0, 4, 0, 0, 8, 0, 7],
    [0, 0, 1, 2, 5, 0, 3, 0, 0]
  ];
  const COMPLETED_SUDOKU = [
    [5, 1, 8, 4, 9, 6, 3, 7, 2],
    [3, 9, 7, 1, 2, 5, 4, 6, 8],
    [4, 6, 2, 8, 7, 3, 1, 9, 5],
    [8, 3, 6, 5, 1, 9, 2, 4, 7],
    [7, 4, 9, 2, 3, 8, 6, 5, 1],
    [2, 5, 1, 6, 4, 7, 8, 3, 9],
    [1, 8, 5, 9, 6, 4, 7, 2, 3],
    [9, 7, 4, 3, 8, 2, 5, 1, 6],
    [6, 2, 3, 7, 5, 1, 9, 8, 4]
  ];
  const EMPTY_SUDOKU = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
  ];

  let getSudoku = function(sudokuMassiveConst: number[][]): Sudoku {
    let sudokuMassive = [];
    for (let i = 0; i < sudokuMassiveConst.length; i++) {
      sudokuMassive[i] = [];
      for (let j = 0; j < sudokuMassiveConst[i].length; j++) {
        sudokuMassive[i][j] = sudokuMassiveConst[i][j];
      }
    }
    let sudoku = new Sudoku(sudokuMassive, [], []);
    sudoku.createSudokuBlocksMassive();

    return sudoku;
  };

  let fillSudokuValidOptions = function(sudoku: Sudoku): Sudoku {
    sudoku.sudokuValidOptions = [];
    for (let i = 0; i < 9; i++) {
      sudoku.sudokuValidOptions[i] = [];
      for (let j = 0; j < 9; j++) {
        sudoku.sudokuValidOptions[i][j] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
      }
    }

    return sudoku;
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SudokuSolverService]
    });

    service = TestBed.get(SudokuSolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('fillSudokuValidOptions should return ValidOptions object with correct values', () => {
    let sudoku = getSudoku(CORRECT_SUDOKU);
    let result = service.fillSudokuValidOptions(sudoku);

    expect(result.validOptions[0][0]).toEqual([1, 3, 5, 9]);
  });

  it('isSudokuCorrect should return true if correct', () => {
    let sudoku = getSudoku(CORRECT_SUDOKU);
    sudoku.sudokuValidOptions = service.fillSudokuValidOptions(sudoku).validOptions;

    expect(service.isSudokuCorrect(sudoku)).toEqual(true);
  });

  it('isSudokuCorrect should return massive if two numbers not correct', () => {
    let sudoku = getSudoku(BAD_SUDOKU);
    sudoku.sudokuValidOptions = service.fillSudokuValidOptions(sudoku).validOptions;

    expect(service.isSudokuCorrect(sudoku)).toEqual([[3, 1], [3, 4]]);
  });

  it('isSudokuCorrect should return string if more than one solution', () => {
    let sudoku = getSudoku(EMPTY_SUDOKU);
    sudoku.sudokuValidOptions = service.fillSudokuValidOptions(sudoku).validOptions;

    expect(service.isSudokuCorrect(sudoku)).toEqual('more than one solution');
  });

  it('isSudokuCorrect should return string if no solutions', () => {
    let sudoku = getSudoku(UNSOLVABLE_SUDOKU);
    sudoku.sudokuValidOptions = service.fillSudokuValidOptions(sudoku).validOptions;

    expect(service.isSudokuCorrect(sudoku)).toEqual('no solution');
  });

  it('nextStep should return correct object if next step available', () => {
    let sudoku = getSudoku(CORRECT_SUDOKU);
    sudoku = fillSudokuValidOptions(sudoku);
    sudoku.sudokuValidOptions[0][1] = [1];

    expect(service.nextStep(sudoku)['message']).toEqual("single in space");
    expect(service.nextStep(sudoku)['numberToEnter']).toEqual(1);
    expect(service.nextStep(sudoku)['row']).toEqual(0);
    expect(service.nextStep(sudoku)['column']).toEqual(1);
  });

  it('nextStep should return correct object if sudoku solved', () => {
    let sudoku = getSudoku(COMPLETED_SUDOKU);
    sudoku = fillSudokuValidOptions(sudoku);

    expect(service.nextStep(sudoku)['message']).toEqual("sudoku solved");
  });
});
