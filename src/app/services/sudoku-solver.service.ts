import {Injectable} from '@angular/core';
import {Sudoku} from '../models/sudoku';
import {ValidOptions} from '../models/valid-options'
import * as _ from 'lodash'

@Injectable()
export class SudokuSolverService {

  constructor() {
  }

  public isSudokuCorrect(sudoku: Sudoku): number[][] | string | true {
    let sudokuMassive = sudoku.sudokuMassive;
    let sudokuBlocksMassive = sudoku.sudokuBlocksMassive;
    let sudokuValidOptions = sudoku.sudokuValidOptions;

    if (this.isSudokuSolved(sudokuMassive)) {
      return 'sudoku solved';
    }

    let duplicateCheck = this.hasDuplicatesInRows(sudokuMassive);
    if (duplicateCheck) {
      return duplicateCheck;
    }

    duplicateCheck = this.hasDuplicatesInColumns(sudokuMassive);
    if (duplicateCheck) {
      return duplicateCheck;
    }

    duplicateCheck = this.hasDuplicatesInBlocks(sudokuBlocksMassive);
    if (duplicateCheck) {
      return duplicateCheck;
    }

    if (this.hasLessNumbersThanRequired(sudokuMassive)) {
      return 'more than one solution';
    }

    if (this.hasNoOptionsInSpace(sudokuValidOptions)) {
      return 'no solution';
    }

    if (this.hasNoSpaceForNumber(sudokuValidOptions, sudokuBlocksMassive)) {
      return 'no solution';
    }

    return true;
  }

  public fillSudokuValidOptions(sudoku: Sudoku): ValidOptions {
    let validOptions = new ValidOptions();

    for (let row = 0; row < sudoku.sudokuMassive.length; row++) {
      validOptions.validOptions[row] = [];

      for (let column = 0; column < sudoku.sudokuMassive[row].length; column++) {
        validOptions.validOptions[row][column] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        validOptions.validOptions[row][column] = this.checkValidOptions(
          sudoku,
          row,
          column,
          validOptions.validOptions[row][column]
        );
      }
    }

    validOptions = this.checkTuples(sudoku, validOptions);
    return validOptions;
  }

  public nextStep(sudoku: Sudoku): Object {
    for (let row = 0; row < sudoku.sudokuMassive.length; row++) {
      for (let column = 0; column < sudoku.sudokuMassive[row].length; column++) {

        if (!sudoku.sudokuMassive[row][column]) {
          return this.findNextStep(sudoku);
        }
      }
    }

    return {
      'message': 'sudoku solved'
    }
  }

  public checkForValidSolutions(sudoku) {
    let sudokuCopy = _.cloneDeep(sudoku);

    let nextBacktrackStep = this.findNextBacktrackStep(sudoku);
    let numberOfSolutions = this.findSolutionsByBacktrack(
      sudokuCopy,
      nextBacktrackStep[0],
      nextBacktrackStep[1],
      0
    );

    if (numberOfSolutions === 0) {
      return 'no solution';
    } else if (numberOfSolutions > 1) {
      return 'more than one solution'
    }

    return true;
  }

  private isSudokuSolved(sudokuMassive) {
    for (let row = 0; row < sudokuMassive.length; row++) {
      for (let column = 0; column < sudokuMassive[row].length; column++) {

        if (!sudokuMassive[row][column]) {
          return false;
        }
      }
    }
    return true;
  }

  private hasDuplicatesInRows(sudokuMassive) {
    let numberToCompare = 0;

    for (let row = 0; row < sudokuMassive.length; row++) {
      for (let columnFirst = 0; columnFirst < sudokuMassive[row].length; columnFirst++) {
        numberToCompare = sudokuMassive[row][columnFirst];

        for (let columnSecond = columnFirst + 1; columnSecond < sudokuMassive[row].length; columnSecond++) {
          if (!numberToCompare) {
            break;
          }

          if (numberToCompare === sudokuMassive[row][columnSecond]) {
            return [[row, columnFirst], [row, columnSecond]];
          }
        }
      }
    }

    return false;
  }

  private hasDuplicatesInColumns(sudokuMassive) {
    let numberToCompare = 0;

    for (let column = 0; column < sudokuMassive[0].length; column++) {
      for (let rowFirst = 0; rowFirst < sudokuMassive.length; rowFirst++) {
        numberToCompare = sudokuMassive[rowFirst][column];

        for (let rowSecond = rowFirst + 1; rowSecond < sudokuMassive[rowFirst].length; rowSecond++) {
          if (!numberToCompare) {
            break;
          }

          if (numberToCompare === sudokuMassive[rowSecond][column]) {
            return [[rowFirst, column], [rowSecond, column]];
          }
        }
      }
    }

    return false;
  }

  private hasDuplicatesInBlocks(sudokuBlocksMassive) {
    let numberToCompare = 0;

    for (let block in sudokuBlocksMassive) {
      for (let numberFirst = 0; numberFirst < sudokuBlocksMassive[block].length; numberFirst++) {
        numberToCompare = sudokuBlocksMassive[block][numberFirst];

        for (let numberSecond = numberFirst + 1; numberSecond < sudokuBlocksMassive[block].length; numberSecond++) {
          if (!numberToCompare) {
            break;
          }

          if (numberToCompare === sudokuBlocksMassive[block][numberSecond]) {
            let firstNumber = this.getIndexByBlockIndex(parseInt(block), numberFirst);
            let secondNumber = this.getIndexByBlockIndex(parseInt(block), numberSecond);

            return [firstNumber, secondNumber];
          }
        }
      }
    }

    return false;
  }

  private hasLessNumbersThanRequired(sudokuMassive) {
    let numberCount = 0;
    for (let row = 0; row < sudokuMassive.length; row++) {
      for (let column = 0; column < sudokuMassive[row].length; column++) {
        if (sudokuMassive[row][column]) {
          numberCount++;
          if (numberCount > 16) {
            return false;
          }
        }
      }
    }

    return true;
  }

  private hasNoOptionsInSpace(validOptions) {
    for (let row = 0; row < validOptions.length; row++) {
      for (let column = 0; column < validOptions[row].length; column++) {
        if (validOptions[row][column].length === 0) {
          return true;
        }
      }
    }
    return false;
  }

  private hasNoSpaceForNumber(validOptions, sudokuBlocksMassive) {
    for (let row = 0; row < validOptions.length; row++) {
      for (let number = 1; number <= 9; number++) {
        let hasSpace = false;
        for (let column = 0; column < validOptions[row].length; column++) {
          if (validOptions[row][column].includes(number)) {
            hasSpace = true;
          }
        }
        if (!hasSpace) {
          return true;
        }
      }
    }

    for (let column = 0; column < validOptions[0].length; column++) {
      for (let number = 1; number <= 9; number++) {
        let hasSpace = false;
        for (let row = 0; row < validOptions.length; row++) {
          if (validOptions[row][column].includes(number)) {
            hasSpace = true;
          }
        }
        if (!hasSpace) {
          return true;
        }
      }
    }

    for (let block = 0; block < sudokuBlocksMassive.length; block++) {
      for (let number = 1; number <= 9; number++) {
        let hasSpace = false;
        for (let indexInBlock = 0; indexInBlock < sudokuBlocksMassive[block].length; indexInBlock++) {
          let validOptionsIndex = this.getIndexByBlockIndex(block, indexInBlock);
          if (validOptions[validOptionsIndex[0]][validOptionsIndex[1]].includes(number)) {
            hasSpace = true;
          }
        }
        if (!hasSpace) {
          return true;
        }
      }
    }

    return false;
  }

  private checkValidOptions(sudoku, row, column, validOptions?) {
    validOptions = validOptions || sudoku.sudokuValidOptions[row][column];

    validOptions = this.checkRowValidOptions(sudoku.sudokuMassive, validOptions, row, column);
    validOptions = this.checkColumnValidOptions(sudoku.sudokuMassive, validOptions, row, column);
    validOptions = this.checkBlockValidOptions(sudoku.sudokuBlocksMassive, validOptions, row, column);

    return validOptions;
  }

  private checkRowValidOptions(sudokuMassive, currentValidOptions, row, column) {
    let newValidOptions = currentValidOptions;

    if (sudokuMassive[row][column] > 0) {
      newValidOptions = [sudokuMassive[row][column]];
      return newValidOptions;
    }

    for (let col = 0; col < sudokuMassive[row].length; col++) {

      if (col !== column && sudokuMassive[row][col] > 0) {
        let index = newValidOptions.indexOf(sudokuMassive[row][col]);

        if (index !== -1) {
          newValidOptions.splice(index, 1);
        }
      }
    }

    return newValidOptions;
  }

  private checkColumnValidOptions(sudokuMassive, currentValidOptions, row, column) {
    let newValidOptions = currentValidOptions;

    if (sudokuMassive[row][column] > 0) {
      newValidOptions = [sudokuMassive[row][column]];
      return newValidOptions;
    }

    for (let r = 0; r < sudokuMassive.length; r++) {
      if (r !== row && sudokuMassive[r][column] > 0) {
        let index = newValidOptions.indexOf(sudokuMassive[r][column]);

        if (index !== -1) {
          newValidOptions.splice(index, 1);
        }
      }
    }

    return newValidOptions;
  }

  private checkBlockValidOptions(sudokuBlocksMassive, currentValidOptions, row, column) {
    let newValidOptions = currentValidOptions;
    let sudokuBlockIndex = this.getBlockIndexByIndex(row, column);
    let blockIndex = sudokuBlockIndex[0];
    let numberIndex = sudokuBlockIndex[1];

    if (sudokuBlocksMassive[blockIndex][numberIndex] > 0) {
      newValidOptions = [sudokuBlocksMassive[blockIndex][numberIndex]];
      return newValidOptions;
    }

    for (let number = 0; number < sudokuBlocksMassive[blockIndex].length; number++) {

      if (number !== numberIndex && sudokuBlocksMassive[blockIndex][number] > 0) {
        let index = newValidOptions.indexOf(sudokuBlocksMassive[blockIndex][number]);

        if (index !== -1) {
          newValidOptions.splice(index, 1);
        }
      }
    }

    return newValidOptions;
  }

  private checkTuples(sudoku, validOptions) {

    validOptions = this.checkNakedTuplesInRows(validOptions);
    if (validOptions.tuplesFound.length !== 0) {
      return validOptions;
    }

    validOptions = this.checkNakedTuplesInColumns(validOptions);
    if (validOptions.tuplesFound.length !== 0) {
      return validOptions;
    }

    validOptions = this.checkNakedTuplesInBlocks(sudoku, validOptions);

    return validOptions;
  }

  private checkNakedTuplesInRows(validOptions) {
    validOptions.tuplesFound = [];

    for (let tupleSize = 2; tupleSize < 5; tupleSize++) {

      for (let row = 0; row < validOptions.validOptions.length; row++) {
        let possibleTuples = [];

        for (let column = 0; column < validOptions.validOptions[row].length; column++) {
          if (validOptions.validOptions[row][column].length === tupleSize) {
            possibleTuples.push(validOptions.validOptions[row][column]);
          }
        }
        this.findTuplesInRow(validOptions, possibleTuples, tupleSize, row)
      }
    }

    return validOptions;
  }

  private checkNakedTuplesInColumns(validOptions) {
    validOptions.tuplesFound = [];

    for (let tupleSize = 2; tupleSize < 5; tupleSize++) {

      for (let column = 0; column < validOptions.validOptions[0].length; column++) {
        let possibleTuples = [];

        for (let row = 0; row < validOptions.validOptions.length; row++) {
          if (validOptions.validOptions[row][column].length === tupleSize) {
            possibleTuples.push(validOptions.validOptions[row][column]);
          }
        }
        validOptions = this.findTuplesInColumn(validOptions, possibleTuples, tupleSize, column)
      }
    }

    return validOptions;
  }

  private checkNakedTuplesInBlocks(sudoku, validOptions) {
    validOptions.tuplesFound = [];

    for (let tupleSize = 2; tupleSize < 5; tupleSize++) {

      for (let blockIndex = 0; blockIndex < sudoku.sudokuBlocksMassive.length; blockIndex++) {
        let possibleTuples = [];

        for (let column = 0; column < sudoku.sudokuBlocksMassive[blockIndex].length; column++) {
          let index = this.getIndexByBlockIndex(blockIndex, column);

          if (validOptions.validOptions[index[0]][index[1]].length === tupleSize) {
            possibleTuples.push(validOptions.validOptions[index[0]][index[1]]);
          }
        }
        validOptions = this.findTuplesInBlock(sudoku, validOptions, possibleTuples, tupleSize, blockIndex)
      }
    }

    return validOptions;
  }

  private findNextStep(sudoku) {
    let nextStep = this.findSingleInSpace(sudoku);
    if (nextStep) {
      return nextStep;
    }

    nextStep = this.findSingleInBlock(sudoku);
    if (nextStep) {
      return nextStep;
    }

    nextStep = this.findSingleInRow(sudoku);
    if (nextStep) {
      return nextStep;
    }

    nextStep = this.findSingleInColumn(sudoku);
    if (nextStep) {
      return nextStep;
    }

    nextStep = this.findByBacktrack(sudoku);
    if (nextStep) {
      return nextStep;
    }

    return {
      'message': 'next step not found'
    };
  }

  private findSingleInSpace(sudoku) {
    for (let row = 0; row < sudoku.sudokuValidOptions.length; row++) {
      for (let column = 0; column < sudoku.sudokuValidOptions[row].length; column++) {

        if (!sudoku.sudokuMassive[row][column]) {
          if (sudoku.sudokuValidOptions[row][column].length === 1) {
            return {
              'message': 'single in space',
              'numberToEnter': sudoku.sudokuValidOptions[row][column][0],
              'row': row,
              'column': column
            };
          }
        }
      }
    }

    return null;
  }

  private findSingleInRow(sudoku) {
    for (let row = 0; row < sudoku.sudokuValidOptions.length; row++) {
      let numberCount = [0, 0, 0, 0, 0, 0, 0, 0, 0];

      for (let column = 0; column < sudoku.sudokuValidOptions[row].length; column++) {
        if (!sudoku.sudokuMassive[row][column]) {

          for (let number = 0; number < sudoku.sudokuValidOptions[row][column].length; number++) {
            numberCount[sudoku.sudokuValidOptions[row][column][number] - 1]++;
          }
        }
      }
      for (let number = 0; number < numberCount.length; number++) {
        if (numberCount[number] === 1) {

          for (let column = 0; column < sudoku.sudokuValidOptions[row].length; column++) {
            if (sudoku.sudokuValidOptions[row][column].indexOf(number + 1) !== -1) {
              return {
                'message': 'single in row',
                'numberToEnter': number + 1,
                'row': row,
                'column': column
              };
            }
          }
        }
      }
    }

    return null;
  }

  private findSingleInColumn(sudoku) {
    for (let column = 0; column < sudoku.sudokuValidOptions[0].length; column++) {
      let numberCount = [0, 0, 0, 0, 0, 0, 0, 0, 0];

      for (let row = 0; row < sudoku.sudokuValidOptions.length; row++) {
        if (!sudoku.sudokuMassive[row][column]) {

          for (let number = 0; number < sudoku.sudokuValidOptions[row][column].length; number++) {
            numberCount[sudoku.sudokuValidOptions[row][column][number] - 1]++;
          }
        }
      }

      for (let number = 0; number < numberCount.length; number++) {
        if (numberCount[number] === 1) {

          for (let row = 0; row < sudoku.sudokuValidOptions.length; row++) {
            if (sudoku.sudokuValidOptions[row][column].indexOf(number + 1) !== -1) {
              return {
                'message': 'single in column',
                'numberToEnter': number + 1,
                'row': row,
                'column': column
              };
            }
          }
        }
      }
    }

    return null;
  }

  private findSingleInBlock(sudoku) {
    for (let row = 0; row < sudoku.sudokuBlocksMassive.length; row++) {
      let numberCount = [0, 0, 0, 0, 0, 0, 0, 0, 0];

      for (let column = 0; column < sudoku.sudokuBlocksMassive[row].length; column++) {
        if (!sudoku.sudokuBlocksMassive[row][column]) {
          let index = this.getIndexByBlockIndex(row, column);

          for (let number = 0; number < sudoku.sudokuValidOptions[index[0]][index[1]].length; number++) {
            numberCount[sudoku.sudokuValidOptions[index[0]][index[1]][number] - 1]++;
          }
        }
      }

      for (let number = 0; number < numberCount.length; number++) {
        if (numberCount[number] === 1) {

          for (let column = 0; column < sudoku.sudokuBlocksMassive[row].length; column++) {
            let index = this.getIndexByBlockIndex(row, column);

            if (sudoku.sudokuValidOptions[index[0]][index[1]].indexOf(number + 1) !== -1) {
              return {
                'message': 'single in block',
                'numberToEnter': number + 1,
                'row': index[0],
                'column': index[1]
              };
            }
          }
        }
      }
    }

    return null;
  }

  private getIndexByBlockIndex(blockIndex, numberIndex) {
    let row = (Math.floor(blockIndex / 3)) * 3 + Math.floor(numberIndex / 3);
    let column = (blockIndex % 3) * 3 + numberIndex % 3;

    return [row, column];
  }

  private getBlockIndexByIndex(row, column) {
    let blockIndex = (Math.floor(row / 3)) * 3 + Math.floor(column / 3);
    let numberIndex = (row % 3) * 3 + column % 3;

    return [blockIndex, numberIndex];
  }

  private findTuplesInRow(validOptions, possibleTuples, tupleSize, row) {
    for (let firstTuple = 0; firstTuple < possibleTuples.length; firstTuple++) {
      let tupleCount = 1;
      for (let secondTuple = firstTuple + 1; secondTuple < possibleTuples.length; secondTuple++) {
        let isEqual = true;
        for (let number = 0; number < possibleTuples[firstTuple].length; number++) {
          if (possibleTuples[firstTuple][number] !== possibleTuples[secondTuple][number]) {
            isEqual = false;
            break;
          }
        }
        if (isEqual) {
          tupleCount++;
        }

        if (tupleCount === tupleSize) {
          validOptions = this.updateValidOptionsWithTupleInRow(
            validOptions,
            tupleCount,
            possibleTuples[firstTuple],
            row
          );
        }
      }
    }

    return validOptions;
  }

  private findTuplesInColumn(validOptions, possibleTuples, tupleSize, column) {
    for (let firstTuple = 0; firstTuple < possibleTuples.length; firstTuple++) {
      let tupleCount = 1;
      for (let secondTuple = firstTuple + 1; secondTuple < possibleTuples.length; secondTuple++) {
        let isEqual = true;
        for (let number = 0; number < possibleTuples.length; number++) {
          if (possibleTuples[firstTuple][number] !== possibleTuples[secondTuple][number]) {
            isEqual = false;
          }
        }
        if (isEqual) {
          tupleCount++;
        }

        if (tupleCount === tupleSize) {
          validOptions = this.updateValidOptionsWithTupleInColumn(
            validOptions,
            tupleCount,
            possibleTuples[firstTuple],
            column
          )
        }
      }
    }

    return validOptions;
  }

  private findTuplesInBlock(sudoku, validOptions, possibleTuples, tupleSize, blockIndex) {
    for (let firstTuple = 0; firstTuple < possibleTuples.length; firstTuple++) {
      let tupleCount = 1;
      for (let nextTuple = firstTuple + 1; nextTuple < possibleTuples.length; nextTuple++) {
        let equal = true;
        for (let number = 0; number < possibleTuples.length; number++) {
          if (possibleTuples[firstTuple][number] !== possibleTuples[nextTuple][number]) {
            equal = false;
          }
        }
        if (equal) {
          tupleCount++;
        }

        if (tupleCount === tupleSize) {
          validOptions = this.updateValidOptionsWithTupleInBlock(
            sudoku,
            validOptions,
            tupleCount,
            possibleTuples[firstTuple],
            blockIndex
          )
        }
      }
    }

    return validOptions;
  }

  private updateValidOptionsWithTupleInRow(validOptions, tupleCount, tuple, row) {
    validOptions.tuplesFound.push(tupleCount);
    validOptions.tuplesFound.push(tuple);
    validOptions.tuplesFound.push("row");
    validOptions.tuplesFound.push(row);

    for (let column = 0; column < validOptions.validOptions[row].length; column++) {

      let optionsEqual = this.areOptionsEqualToTuple(validOptions.validOptions[row][column], tuple);

      if (!optionsEqual) {
        validOptions.validOptions[row][column] = this.removeOptionsNotInTuple(validOptions.validOptions[row][column], tuple);

      } else {
        validOptions.tuplesFound.push([row, column]);
      }
    }

    return validOptions;
  }

  private updateValidOptionsWithTupleInColumn(validOptions, tupleCount, tuple, column) {
    validOptions.tuplesFound.push(tupleCount);
    validOptions.tuplesFound.push(tuple);
    validOptions.tuplesFound.push("column");
    validOptions.tuplesFound.push(column);

    for (let row = 0; row < validOptions.validOptions.length; row++) {

      let optionsEqual = this.areOptionsEqualToTuple(validOptions.validOptions[row][column], tuple);

      if (!optionsEqual) {
        validOptions.validOptions[row][column] = this.removeOptionsNotInTuple(validOptions.validOptions[row][column], tuple);
      } else {
        validOptions.tuplesFound.push([row, column]);
      }
    }

    return validOptions;
  }

  private updateValidOptionsWithTupleInBlock(sudoku, validOptions, tupleCount, tuple, blockIndex) {
    validOptions.tuplesFound.push(tupleCount);
    validOptions.tuplesFound.push(tuple);
    validOptions.tuplesFound.push("block");
    validOptions.tuplesFound.push(blockIndex);

    for (let column = 0; column < sudoku.sudokuBlocksMassive[blockIndex].length; column++) {
      let index = this.getIndexByBlockIndex(blockIndex, column);

      let optionsEqual = this.areOptionsEqualToTuple(validOptions.validOptions[index[0]][index[1]], tuple);

      if (!optionsEqual) {
        validOptions.validOptions[index[0]][index[1]] = this.removeOptionsNotInTuple(
          validOptions.validOptions[index[0]][index[1]],
          tuple
        );
      } else {
        validOptions.tuplesFound.push([blockIndex, column]);
      }
    }

    return validOptions;
  }

  private areOptionsEqualToTuple(validOptionsSlot, tuple) {
    if (validOptionsSlot.length === tuple.length) {
      for (let number = 0; number < validOptionsSlot.length; number++) {
        if (validOptionsSlot[number] !== tuple[number]) {
          return false
        }
      }
    } else {
      return false;
    }

    return true;
  }

  private removeOptionsNotInTuple(validOptionsSlot, tuple) {
    for (let number = 0; number < validOptionsSlot.length; number++) {
      for (let tupleNumber = 0; tupleNumber < tuple.length; tupleNumber++) {
        if (validOptionsSlot[number] === tuple[tupleNumber]) {
          validOptionsSlot.splice(number, 1);
          number = number - 1;
        }
      }
    }
    return validOptionsSlot;
  }

  private findByBacktrack(sudoku) {
    let sudokuCopy = _.cloneDeep(sudoku);
    let backtrackStart = this.getBacktrackStart(sudokuCopy);

    let sudokuSolved = this.setValidNumber(sudokuCopy, backtrackStart[0], backtrackStart[1]);
    if (sudokuSolved) {
      return {
        'message': 'found by backtrack',
        'numberToEnter': sudokuCopy.sudokuMassive[backtrackStart[0]][backtrackStart[1]],
        'row': backtrackStart[0],
        'column': backtrackStart[1]
      }
    }

    return null;
  }

  private getBacktrackStart(sudoku) {
    let backtrackStart = [];
    let row = 0;
    let column = 0;
    let numbersInSpace = 2;

    while (numbersInSpace <= 9 && backtrackStart.length === 0) {
      while (row < sudoku.sudokuValidOptions.length && backtrackStart.length === 0) {
        while (column < sudoku.sudokuValidOptions[row].length && backtrackStart.length === 0) {
          if (sudoku.sudokuValidOptions[row][column].length === numbersInSpace) {
            backtrackStart = [
              row,
              column
            ];
          }
          column++;
        }
        column = 0;
        row++;
      }
      row = 0;
      column = 0;
      numbersInSpace++;
    }

    return backtrackStart;
  }

  private setValidNumber(sudoku, row, column) {
    for (let number = 1; number <= 9; number++) {
      if (this.isNumberValid(number, sudoku, row, column)) {
        sudoku.updateSudoku(row, column, number);
        let nextBacktrackStep = this.findNextBacktrackStep(sudoku);
        if (nextBacktrackStep && this.setValidNumber(sudoku, nextBacktrackStep[0], nextBacktrackStep[1])) {
          return true
        } else if (!nextBacktrackStep) {
          return true;
        } else {
          sudoku.updateSudoku(row, column, 0);
        }
      }
    }
    return false;
  }

  private isNumberValid(number, sudoku, row, column) {
    for (let sudokuColumn = 0; sudokuColumn < sudoku.sudokuMassive[row].length; sudokuColumn++) {
      if (sudoku.sudokuMassive[row][sudokuColumn] === number) {
        return false;
      }
    }

    for (let sudokuRow = 0; sudokuRow < sudoku.sudokuMassive.length; sudokuRow++) {
      if (sudoku.sudokuMassive[sudokuRow][column] === number) {
        return false;
      }
    }

    let blockIndex = this.getBlockIndexByIndex(row, column);
    for (let numberInBlock = 0; numberInBlock < sudoku.sudokuBlocksMassive[blockIndex[0]].length; numberInBlock++) {
      if (sudoku.sudokuBlocksMassive[blockIndex[0]][numberInBlock] === number) {
        return false;
      }
    }

    return true;
  }

  private findNextBacktrackStep(sudoku) {
    for (let row = 0; row < sudoku.sudokuMassive.length; row++) {
      for (let column = 0; column < sudoku.sudokuMassive[row].length; column++) {
        if (!sudoku.sudokuMassive[row][column]) {
          return [row, column];
        }
      }
    }
    return false;
  }

  private findSolutionsByBacktrack(sudoku, row, column, numberOfSolutions) {
    let currentNumberOfSolutions = numberOfSolutions;
    for (let number = 1; number <= 9; number++) {
      if (this.isNumberValid(number, sudoku, row, column)) {
        sudoku.updateSudoku(row, column, number);
        let nextBacktrackStep = this.findNextBacktrackStep(sudoku);
        if (nextBacktrackStep) {
          currentNumberOfSolutions = this.findSolutionsByBacktrack(
            sudoku,
            nextBacktrackStep[0],
            nextBacktrackStep[1],
            currentNumberOfSolutions
          );
        } else {
          currentNumberOfSolutions++;
          if (currentNumberOfSolutions > 1) {
            return currentNumberOfSolutions;
          }
        }
        sudoku.updateSudoku(row, column, 0);
      }
    }
    return currentNumberOfSolutions;
  }
}
