import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";


import { AppComponent } from './app.component';
import { SudokuTableComponent } from './components/sudoku-table/sudoku-table.component';
import { SudokuSolverService } from "./services/sudoku-solver.service";
import { ValidateMinDirective } from './directives/validate-min.directive';
import { ValidateMaxDirective } from './directives/validate-max.directive';


@NgModule({
  declarations: [
    AppComponent,
    SudokuTableComponent,
    ValidateMinDirective,
    ValidateMaxDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [SudokuSolverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
