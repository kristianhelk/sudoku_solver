import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SudokuTableComponent } from './sudoku-table.component';
import {FormsModule} from "@angular/forms";
import {SudokuSolverService} from "../../services/sudoku-solver.service";

describe('SudokuTableComponent', () => {
  let component: SudokuTableComponent;
  let fixture: ComponentFixture<SudokuTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SudokuTableComponent ],
      imports: [
        FormsModule
      ],
      providers: [
        SudokuSolverService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SudokuTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
