import {Component, OnInit} from '@angular/core';

import {Sudoku} from "../../models/sudoku";
import {SudokuSolverService} from "../../services/sudoku-solver.service";

@Component({
  selector: 'app-sudoku-table',
  templateUrl: './sudoku-table.component.html',
  styleUrls: ['./sudoku-table.component.css']
})
export class SudokuTableComponent implements OnInit {

  constructor(private sudokuSolverService: SudokuSolverService) {
  }

  sudoku = new Sudoku([]);

  sudokuInvalidMessage = null;
  invalidValueFirst = [];
  invalidValueSecond = [];
  highlightedIndex = [];
  hintType = "";
  tuple = [];
  highlightedTuple = [[], [], []];
  nextStepUnavailable = false;


  ngOnInit() {
  }

  nextStepClicked() {
    this.sudoku.createSudokuBlocksMassive();
    this.sudoku.sudokuValidOptions = this.sudokuSolverService.fillSudokuValidOptions(this.sudoku).validOptions;
    if(this.isSudokuCorrect()) {
      this.nextStep();
    }
  }

  nextStep() {
    this.sudoku.createSudokuBlocksMassive();

    let validOptions = this.sudokuSolverService.fillSudokuValidOptions(this.sudoku);
    this.sudoku.sudokuValidOptions = validOptions.validOptions;
    let tuplesFound = validOptions.tuplesFound;

    let nextStep = this.sudokuSolverService.nextStep(this.sudoku);
    if (nextStep['message'] !== "next step not found" && nextStep['message'] !== 'sudoku solved') {
      this.highlightedIndex = [nextStep['row'], nextStep['column']];
      this.sudoku.sudokuMassive[nextStep['row']][nextStep['column']] = nextStep['numberToEnter'];
    } else {
      this.highlightedIndex = [];
      this.nextStepUnavailable = true;
    }

    this.hintType = nextStep['message'];
    if (tuplesFound.length !== 0) {
      this.tuple = tuplesFound;
      for (let i = 4; i < this.tuple.length; i++) {
        this.highlightedTuple[i - 4] = this.tuple[i];
      }
    } else {
      this.tuple = [];
      this.highlightedTuple = [[], [], []];
    }
  }

  solveSudoku() {
    this.sudoku.createSudokuBlocksMassive();
    this.sudoku.sudokuValidOptions = this.sudokuSolverService.fillSudokuValidOptions(this.sudoku).validOptions;
    if(this.isSudokuCorrect()) {
      while (!this.nextStepUnavailable) {
        this.nextStep();
      }
    }
  }

  checkForValidSolutions() {
    this.sudoku.createSudokuBlocksMassive();
    this.sudoku.sudokuValidOptions = this.sudokuSolverService.fillSudokuValidOptions(this.sudoku).validOptions;
    let isSudokuCorrect = this.isSudokuCorrect();
    let isValidSolution = this.sudokuSolverService.checkForValidSolutions(this.sudoku);
    if (isSudokuCorrect && typeof(isValidSolution) === 'boolean') {
      this.hintType = 'one valid solution'
    } else if (typeof(isValidSolution) === 'string') {
      this.sudokuInvalidMessage = isValidSolution;
    }
  }

  isSudokuCorrect() {
    let isSudokuCorrect = this.sudokuSolverService.isSudokuCorrect(this.sudoku);

    if (typeof(isSudokuCorrect) === 'object') {
      this.sudokuInvalidMessage = 'invalid numbers';
      this.invalidValueFirst = isSudokuCorrect[0];
      this.invalidValueSecond = isSudokuCorrect[1];
      return false;
    } else if (typeof(isSudokuCorrect) === 'string' && isSudokuCorrect !== 'sudoku solved') {
      this.invalidValueFirst = [];
      this.invalidValueSecond = [];
      this.sudokuInvalidMessage = isSudokuCorrect;
      return false;
    } else if (isSudokuCorrect === 'sudoku solved') {
      this.invalidValueFirst = [];
      this.invalidValueSecond = [];
      this.sudokuInvalidMessage = null;
      this.highlightedIndex = [];
      this.hintType = 'sudoku solved';
      return false;
    } else {
      this.invalidValueFirst = [];
      this.invalidValueSecond = [];
      this.sudokuInvalidMessage = null;

      return true;
    }
  }

  resetSudoku() {
    this.sudoku = new Sudoku([]);

    this.sudokuInvalidMessage = null;
    this.invalidValueFirst = [];
    this.invalidValueSecond = [];
    this.highlightedIndex = [];
    this.hintType = "";
    this.tuple = [];
    this.highlightedTuple = [[], [], []];
    this.nextStepUnavailable = false;
  }

  isCustomInvalid(x, y) {
    let matchesInvalidValueFirst = this.invalidValueFirst[0] === x && this.invalidValueFirst[1] === y;
    let matchesInvalidValueSecond = this.invalidValueSecond[0] === x && this.invalidValueSecond[1] === y;
    return matchesInvalidValueFirst || matchesInvalidValueSecond;
  }

  isHighlighted(x, y) {
    return this.highlightedIndex[0] == x && this.highlightedIndex[1] == y;
  }

  isHighlightedTuple(x, y) {
    let matchesFirst = this.highlightedTuple[0][0] == x && this.highlightedTuple[0][1] == y;
    let matchesSecond = this.highlightedTuple[1][0] == x && this.highlightedTuple[1][1] == y;
    let matchesThird = this.highlightedTuple[2][0] == x && this.highlightedTuple[2][1] == y;
    return matchesFirst || matchesSecond || matchesThird;
  }
}
