import {Directive, Input} from '@angular/core';
import {NG_VALIDATORS, Validator, FormControl} from "@angular/forms";

@Directive({
  selector: '[maxValue][ngModel]',
  providers: [ {provide: NG_VALIDATORS, useExisting: ValidateMaxDirective, multi: true} ]
})
export class ValidateMaxDirective implements Validator{
  @Input()
  maxValue: number;

  validate(c: FormControl): {[key: string]: any } {
    let v = c.value;

    return (v > this.maxValue) ? {"maxValue": true} : null;
  }

}
